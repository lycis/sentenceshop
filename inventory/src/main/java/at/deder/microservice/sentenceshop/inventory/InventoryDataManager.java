package at.deder.microservice.sentenceshop.inventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class InventoryDataManager {
    public static final String SENTENCESHOP_INVENTORY_BASE = "sentenceshop:inventory:%s";
    private Jedis jedis;

    public InventoryDataManager(Jedis jedis) {
        this.jedis = jedis;
    }

    public Map<String, Integer> fetchAll() {
        Set<String> words = jedis.keys(String.format(SENTENCESHOP_INVENTORY_BASE, "?*"));

        Map<String, Integer> inventory = new HashMap<>();
        words.forEach(w -> {
            String value = jedis.get(w);
            if(!value.equals("I")) {
                inventory.put(w.substring(SENTENCESHOP_INVENTORY_BASE.length()-2), Integer.parseInt(value));
            }
        });

        return inventory;
    }

    public static InventoryDataManager get() {
        String host = Application.getConfiguration().getProperty("redis.host", "localhost");
        int port    = Integer.parseInt(Application.getConfiguration().getProperty("redis.port", "6379"));
        Jedis jedis = new Jedis(host, port);
        return new InventoryDataManager(jedis);
    }

    public void addStock(String word) {
        jedis.incr(String.format(SENTENCESHOP_INVENTORY_BASE, word));
    }

    public void removeStock(String word) {
        jedis.decr(String.format(SENTENCESHOP_INVENTORY_BASE, word));
    }

    public void deactivate(String word) {
        jedis.set(String.format(SENTENCESHOP_INVENTORY_BASE, word), "I");
    }

    public int getStock(String word) {
        String v = jedis.get(String.format(SENTENCESHOP_INVENTORY_BASE, word));
        if(v == null || "I".equalsIgnoreCase(v)) {
            return -1;
        }

        return Integer.parseInt(v);
    }
}
