package at.deder.microservice.sentenceshop.inventory;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class InventoryController {

    private InventoryDataManager idm;

    public InventoryController() {
        idm = InventoryDataManager.get();
    }

    public InventoryController(InventoryDataManager idm) {
        this.idm = idm;
    }

    @RequestMapping(method= RequestMethod.GET, path = "/inventory")
    public Map<String, Integer> getWordList() {
        Map<String, Integer> words = idm.fetchAll();
        return words;
    }

    @RequestMapping(method=RequestMethod.PUT, path = "/inventory/{word}")
    public ResponseEntity newWord(@PathVariable("word") String word) {
        if (checkValidWord(word)) return new ResponseEntity(HttpStatus.BAD_REQUEST);

        idm.addStock(word);
        return new ResponseEntity(HttpStatus.OK);
    }
    @RequestMapping(method=RequestMethod.DELETE, path = "/inventory/{word}")
    public ResponseEntity deleteWord(@PathVariable("word") String word) {
        if (checkValidWord(word)) return new ResponseEntity(HttpStatus.BAD_REQUEST);

        if(idm.getStock(word) <= 0) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        idm.removeStock(word);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.GET, path="/inventory/{word}")
    public ResponseEntity<String> getWord(@PathVariable("word") String word) {
        int stock = idm.getStock(word);
        if(stock < 0) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<String>(String.valueOf(stock), HttpStatus.OK);
    }

    private boolean checkValidWord(@RequestBody String word) {
        if(word == null) {
            return true;
        }

        if(word.isEmpty()) {
            return true;
        }
        return false;
    }

}
