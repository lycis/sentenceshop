package at.deder.microservice.sentenceshop.inventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

@SpringBootApplication
public class Application {
    private static Properties config = new Properties();
    private static Logger log = Logger.getLogger(Application.class.getName());

    public static void main(String[] args) {
        String configFile = "inventory.properties";
        if(args.length > 0) {
            configFile = args[0];
        }

        try(FileInputStream is = new FileInputStream(configFile)) {
            config.load(is);
        } catch (IOException e) {
            log.log(Level.SEVERE, String.format("Failed to load configuration: %s, reason: %s - %s", configFile, e.getClass().getSimpleName(), e.getMessage()));
            return;
        }

        SpringApplication.run(Application.class, args);
    }

    public static Properties getConfiguration() {
        Properties clone = new Properties();
        clone.putAll(config);
        return clone;
    }
}