package at.deder.microservice.sentenceshop.inventory;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class InventoryControllerTest {
    private InventoryDataManager mockIdm = mock(InventoryDataManager.class);

    @Test
    public void getWordList() {
        InventoryController controller = new InventoryController(mockIdm);

        Map<String, Integer> words = new HashMap<>();
        words.put("a", 1);
        words.put("green", 2);
        words.put("cat", 3);
        doReturn(words).when(mockIdm).fetchAll();

        Map<String, Integer> result = controller.getWordList();
        assertThat(result.size()).isEqualTo(words.size());
        result.forEach((k,v) -> assertThat(words.get(k)).isEqualTo(v));
    }
}
