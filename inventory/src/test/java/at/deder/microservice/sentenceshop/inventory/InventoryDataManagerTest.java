package at.deder.microservice.sentenceshop.inventory;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class InventoryDataManagerTest {
    Jedis mockJedis = mock(Jedis.class);

    @Test
    public void fetchAll() {
        InventoryDataManager idm = new InventoryDataManager(mockJedis);

        Map<String, String> words = new HashMap<>();
        words.put("cat", "1");
        words.put("fish", "5");
        words.put("dog", "0");
        words.put("foo", "I");
        doReturn(words.keySet().stream().map(w -> String.format("sentenceshop:inventory:%s", w)).collect(Collectors.toSet())).when(mockJedis).keys(Mockito.eq("sentenceshop:inventory:?*"));
        words.forEach((k, v) -> doReturn(v).when(mockJedis).get(String.format("sentenceshop:inventory:%s", k)));

        Map<String, Integer> result = idm.fetchAll();
        result.forEach((k,v) -> {
            assertThat(words.get(k)).isNotEqualToIgnoringCase("I");
            assertThat(words.get(k)).isEqualTo(v.toString());
        });
    }
}
